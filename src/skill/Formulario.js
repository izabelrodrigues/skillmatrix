import React, { Component } from 'react';
import { Collapse } from 'reactstrap';
import { Card, CardBody } from 'reactstrap';
import { Form, Input } from 'reactstrap';
import CustomButton from '../componentes/CustomButton';
import CustomInput from '../componentes/CustomInput';
import RodapeFormButton from "../componentes/RodapeFormButton";
import $ from 'jquery';
import PubSub from 'pubsub-js';
import CustomAlert from '../componentes/CustomAlert';


class Formulario extends Component {

    constructor() {
        super();
        this.state = { collapse: false, id: '', descricao: '', observacao: '', msg: '', color: '' };
        this.setId = this.setId.bind(this);
        this.setDescricao = this.setDescricao.bind(this);
        this.setObservacao = this.setObservacao.bind(this);
        this.toggle = this.toggle.bind(this);
        this.limpar = this.limpar.bind(this);
        this.configMsgFeedBack = this.configMsgFeedBack.bind(this);

        PubSub.subscribe('captura-skill-to-edit', function (topico, object) {
            this.setState({ id: object.id, descricao: object.descricao, observacao: object.observacao, collapse: true })
        }.bind(this));
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    enviaForm(event) {
        event.preventDefault();
        let url;
        let method;
        let data;
        if (this.state.id === '') {
            url = 'http://localhost:9292/skill-api/skill/create';
            method = 'POST';
            data = JSON.stringify({ descricao: this.state.descricao, observacao: this.state.observacao })
        } else {
            url = 'http://localhost:9292/skill-api/skill/update';
            method = 'PUT';
            data = JSON.stringify({ id: this.state.id, descricao: this.state.descricao, observacao: this.state.observacao })
        }
        $.ajax({         
            url: url,
            headers: {
                'Accept-Language': navigator.language,
                'Content-Type':'application/json'
            },
            contentType: 'application/json',
            dataType: 'json',
            type: method,
            data: data,
            success: function (resposta) {
                let msgAction;
                let msgParam;
                if(method === 'POST'){
                    msgAction = " adicionada";
                    msgParam = resposta.descricao;
                } else if( method === 'PUT'){
                    msgParam = resposta.id;
                    msgAction = " alterada";                  
                }
                let msgFeedback = 'Skill "' + msgParam +'"' + msgAction + ' com sucesso.';
                this.configMsgFeedBack(msgFeedback, 'success');
                PubSub.publish('atualiza-lista-skill');
                this.limpar();
            }.bind(this),
            error: function (resposta) {
                let msg = resposta.responseText.replace("[", "").replace("]","");
                this.configMsgFeedBack(msg, 'danger');
                this.limpar();
            }.bind(this)

        });
    }

    setId(event) {
        this.setState({ id: event.target.value });
    }

    setDescricao(event) {
        this.setState({ descricao: event.target.value });
    }
    setObservacao(event) {
        this.setState({ observacao: event.target.value });
    }
    limpar() {
        this.setState({ descricao: '', observacao: '' });
    }

    configMsgFeedBack(msg, color) {
        this.setState({ msg: msg });
        this.setState({ color: color });
        $("#msg").fadeTo(3500, 900).slideUp(900, function () {
            $("#msg").slideUp(900);
        });
    }

    render() {

        return (
            <div>
                <CustomButton color="primary" type="button" className="oi oi-plus" labelButton="Novo" onclick={this.toggle} />
                <span className="space-span" />

                <div className="float-right">
                    <Input type="text" id="search" placeholder="pesquisar" />
                </div>
                <br /><br />
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardBody>
                            <CustomAlert id="msg" color={this.state.color} msg={this.state.msg} />
                            <Form id="cadastro-skill" method="post" onSubmit={this.enviaForm.bind(this)} isopen={this.visible}>
                                <Input hidden id="id" type="text" name="id" value={this.state.id} />
                                <CustomInput id="descricao" label="Descrição:" type="text" name="descricao" value={this.state.descricao} onchange={this.setDescricao} classname="text-uppercase" />
                                <CustomInput id="observacao" label="Observação:" type="textarea" name="observacao" value={this.state.observacao} onchange={this.setObservacao} classname="text-uppercase" />
                                <RodapeFormButton onclickCancel={this.limpar} />
                            </Form>
                            <br />
                        </CardBody>
                    </Card>
                </Collapse>
            </div>
        );
    }
}
export default Formulario;