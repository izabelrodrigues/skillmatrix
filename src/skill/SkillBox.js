import React, { Component } from 'react';
import Listagem from './Listagem';
import $ from 'jquery';
import Formulario from './Formulario';
import PubSub from 'pubsub-js';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import CustomAlert from '../componentes/CustomAlert';

class SkillBox extends Component {

    constructor() {
        super();
        this.state = { columnsList: ["ID", "DESCRIÇÃO", "OBSERVAÇÃO", ""], dataList: [], modal: false, idCapturado:'', color:'' };
        this.carregaLista = this.carregaLista.bind(this);
        this.toggle = this.toggle.bind(this);
        this.toogleYes = this.toogleYes.bind(this);
    }

    componentDidMount() {

        this.carregaLista();

        //Escuta o evento de nova atualização da lista de skill e executa ação
        PubSub.subscribe('atualiza-lista-skill', function (topico) {
            this.carregaLista();
        }.bind(this));

        PubSub.subscribe('captura-skill-to-remove', function (topico, id) {
            this.setState({
                modal: !this.state.modal,
                idCapturado: id
            });
        }.bind(this));
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    toogleYes(){
        this.setState({
            modal: !this.state.modal
        });
        let idRemovido = this.state.idCapturado;
        $.ajax({         
            url: "http://localhost:9292/skill-api/skill/" + idRemovido,
            headers: {
                'Accept-Language': navigator.language,
                'Content-Type':'application/json'
            },
            contentType: 'application/json',
            dataType: 'json',
            type: "DELETE",
            success: function (resposta) {
                
                PubSub.publish('atualiza-lista-skill');
            },
            error: function (resposta) {
                let msg = resposta.responseJSON[0].message;
                this.setState({ msg: msg });
                this.setState({ color: 'danger' });
                $("#msg2").fadeTo(3500, 900).slideUp(900, function () {
                    $("#msg2").slideUp(900);
                });
            }.bind(this)

        });
    }

    carregaLista() {
        $.ajax({
            url: "http://localhost:9292/skill-api/skill/list",
            dataType: 'json',
            success: function (resposta) {
                this.setState({ dataList: resposta });
            }.bind(this),
            error: function (resposta) {
            }
        });
    }
    render() {

        return (
            <div>
                <div>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Confirmar Exclusão</ModalHeader>
                        <ModalBody>
                           Deseja remover a skill?
                        </ModalBody>
                        <ModalFooter>
                            <Button color="success" onClick={this.toogleYes}>Sim</Button>
                            <Button color="danger" onClick={this.toggle}>Não</Button>
                        </ModalFooter>
                    </Modal>
                </div>
                <CustomAlert id="msg2" color={this.state.color} msg={this.state.msg} />
                <Formulario msg={this.state.msg} />
                <Listagem columnsList={this.state.columnsList} dataList={this.state.dataList} />
            </div>
        );
    }
}

export default SkillBox;
