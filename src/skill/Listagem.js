import React, { Component } from 'react';
import { Table } from 'reactstrap';
import PubSub from 'pubsub-js';

class Listagem extends Component {

    render() {
        return (
            <div>
                <Table striped>
                    <thead>
                        <tr>{
                            this.props.columnsList.map(function (column, index) {
                                return (
                                    <th key={index}>{column}</th>
                                );
                            })
                        }
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.dataList.map(function (skill) {

                                let alterar = function (skill) {
                                    PubSub.publish('captura-skill-to-edit', skill);
                                }

                                let remover = function (id) {
                                   PubSub.publish('captura-skill-to-remove', id);
                                }

                                return (

                                    <tr key={skill.id}>
                                        <td>{skill.id}</td>
                                        <td>{skill.descricao}</td>
                                        <td>{skill.observacao}</td>
                                        <td>
                                            <span className="oi oi-pencil" color="light" onClick={function () { alterar(skill) }} />
                                            <span className="space-span"></span>
                                            <span className="oi oi-trash" color="light" onClick={function () { remover(skill.id) }} />
                                        </td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Listagem;