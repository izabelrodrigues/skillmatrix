import React, { Component } from 'react';
import { Button } from 'reactstrap';

class CustomButton extends Component {
    render(){
        return(

            <Button color={this.props.color} size={this.props.size} onClick={this.props.onclick} type={this.props.type}>
                <span className={this.props.className}></span> {this.props.labelButton}
            </Button>
        )
    }
}

export default CustomButton;