import React, { Component } from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

class CustomInput extends Component {
    render(){

        return(
            <FormGroup>
                <Label for={this.props.id}>{this.props.label}</Label>
                <Input className={this.props.classname} type={this.props.type} name={this.props.name} id={this.props.id} value={this.props.value} onChange={this.props.onchange}/>
            </FormGroup>
        )

    }
}

export default CustomInput;