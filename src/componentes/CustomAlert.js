import React, { Component } from 'react';
import { Alert } from 'reactstrap';
class CustomAlert extends Component {
    render(){
        return (
            <Alert id={this.props.id} color={this.props.color}>{this.props.msg}</Alert>
        )
    }
}

export default CustomAlert;