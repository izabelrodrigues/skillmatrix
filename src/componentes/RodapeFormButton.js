import React, { Component } from 'react';
import CustomButton from '../componentes/CustomButton';

class RodapeFormButton extends Component{
    render() {
        return (
            <div id="botoes_form" className="botoes_form"> 
               <CustomButton className="oi oi-check" color="success" type="submit" labelButton="salvar">Salvar</CustomButton>
                <span className="space-span"/>
                <CustomButton className="oi oi-x" color="danger" type="button" labelButton="cancelar" onclick={this.props.onclickCancel}>Cancelar</CustomButton>
            </div>
        )
    }
}

export default RodapeFormButton;
