import React, { Component } from 'react';
import $ from 'jquery';
import { Card, CardTitle, Row, Col, CardFooter, NavLink} from 'reactstrap';
class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = { data: {} };
    }

    componentDidMount() {
        $.ajax({
            url: "http://localhost:9292/skill-api/dashboard",
            dataType: 'json',
            success: function (resposta) {
                this.setState({ data: resposta });
            }.bind(this)
        }
        );

    }
    render() {
        return (
            <div>
            <Row>
               <Col xs="6">
                    <Card body inverse color="danger">
                            <CardTitle tag="h2">
                                <span className="oi oi-book" />
                                <div className="float-right">
                                    Skills: 
                                <span className="space-span" />
                                    {this.state.data.countSkill}
                                </div>
                            </CardTitle>
                            <CardFooter tag="h5">                                
                                <NavLink href="/skill"> 
                                    <span className="oi oi-caret-right"/>
                                    <span className="space-span" />
                                    Exibir detalhes
                                </NavLink>
                            </CardFooter>
                    </Card>
               </Col>
               <Col xs="6">
                <Card body inverse color="primary">
                        <CardTitle tag="h2">
                            <span className="oi oi-people" />
                            <div className="float-right">
                                Usuários: 
                            <span className="space-span" />
                                {this.state.data.countUser}
                            </div>
                        </CardTitle>
                        <CardFooter tag="h5">
                            <NavLink href="/user"> 
                                        <span className="oi oi-caret-right"/>
                                        <span className="space-span" />
                                        Exibir detalhes
                            </NavLink>
                        </CardFooter>
                </Card>
            </Col>
            </Row>
            </div>
        )
    }
}
export default Dashboard;