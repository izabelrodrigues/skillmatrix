import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Skill from './skill/SkillBox';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'open-iconic/font/css/open-iconic-bootstrap.css';
import Dashboard from './dashboard/Dashboard';

ReactDOM.render((
    <Router>
        <App>
                <Switch>            
                    <Route exact path="/" component={Dashboard}/>   
                    <Route path="/skill" component={Skill}/>        
                </Switch>            
        </App>
    </Router>

), document.getElementById('root'));
