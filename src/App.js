import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem, NavLink } from 'reactstrap'

class App extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1><img src={logo} className="App-logo" alt="logo" />Management Skill</h1>
        </header>
        <div>

          <Navbar color="danger" light expand="md">
            <NavbarBrand href="/">DASHBOARD</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml" navbar>
                <NavItem >
                  <NavLink href="/skill">Skills</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/skill">Usuários</NavLink>
                </NavItem>
              </Nav>

            </Collapse>
          </Navbar>
        </div>

        <div id="main" className="content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default App;
